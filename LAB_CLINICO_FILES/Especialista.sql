--DROP FUNCTION NEWESPECIALISTA_01;
CREATE OR REPLACE PROCEDURE NEWESPECIALISTA_02(USERNAME_ NVARCHAR2, PASS_ NVARCHAR2, ID_ROL_ NUMBER, 
                            NOMBRE_ NVARCHAR2, APELLIDOP_ NVARCHAR2, APELLIDOM_ NVARCHAR2, EDAD_ NUMBER, 
                            SEXO_ NVARCHAR2, TURNO_ NVARCHAR2, TELEFONO_ NVARCHAR2, CORREO_ NVARCHAR2,
                            CEDULA_ VARCHAR2, RFC_ VARCHAR2, REFERENCIA1_ VARCHAR2, REFERENCIA2_ VARCHAR2,
                            AREA_ NUMBER)
                            --RETURN VARCHAR2
                            IS
    id_u number := 0;
    id_p number := 0;
    id_e number := 0;
    n_servicios number := 0;
BEGIN
    
    INSERT INTO PERFIL (NOMBRE, APELLIDOP, APELLIDOM, EDAD, SEXO, TURNO, TELEFONO, CORREO) 
    VALUES(NOMBRE_, APELLIDOP_, APELLIDOM_, EDAD_, SEXO_, TURNO_, TELEFONO_, CORREO_);
    -- RETURNING id_perfil INTO id_p;
    -- RETURNING id_perfil INTO :id_p;
    SELECT
        MAX(id_perfil)
    INTO id_p
    FROM PERFIL;

    INSERT INTO USUARIO (USERNAME, PASS, ID_ROL) VALUES(USERNAME_, PASS_, ID_ROL_);
    --RETURNING id_usuario INTO id_u;

    SELECT
        MAX(id_usuario)
    INTO id_u
    FROM usuario;

    
    INSERT INTO ESPECIALISTA (ID_USUARIO, id_perfil, CEDULA, RFC, REFERENCIA1, REFERENCIA2, activo) 
    VALUES(id_u, id_p, CEDULA_, RFC_, REFERENCIA1_, REFERENCIA2_, 1);
    
    SELECT
        MAX(id_especialista)
    INTO id_e
    FROM especialista;

    
    SELECT COUNT(*) into n_servicios FROM SERVICIOS WHERE ID_AREA = 1;

    FOR i IN 1..n_servicios LOOP
        INSERT INTO SERVICIO_ESPECIALISTA(id_especialista, id_servicio, activo) values(id_e,(select id_servicio from servicios where id_area = 1), 1); 
    END LOOP;
    
--    RETURNING id_especialista INTO id_e;
    dbms_output.put_line('ESPECIALISTA REGISTRADO CON EXITO');
    --RETURN 'ESPECIALISTA REGISTRADO CON EXITO';   
 
END;
/







CREATE OR REPLACE FUNCTION ELIMINAR_ESPECIALISTA(id_ number) 
                           RETURN VARCHAR2
                           IS
a number := 0;                           
BEGIN
    
    SELECT e.id_especialista INTO a FROM ESPECIALISTA e WHERE e.id_especialista = id_;
         IF(a<>0) THEN
         DELETE ESPECIALISTA WHERE id_especialista = id_;
             RETURN 'SE HA ELIMINADO EL ESPECIALISTA';
         END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
         RETURN 'NO EXISTE ESPECIALISTA';
      WHEN OTHERS THEN
        UPDATE ESPECIALISTA SET ACTIVO=0 WHERE id_especualista = id_;
        RETURN 'SE HA DESACTIVADO EL ESPECIALISTA';
        
    
END;
/



CREATE OR REPLACE FUNCTION OBTENER_SERVICIOS(id_ number)
return 