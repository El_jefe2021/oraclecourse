/*INSERCION DE LABORATORIO*/
INSERT INTO LABORATORIO (nombre_laboratorio, direccion, telefono, correo) 
VALUES ('Angeles Apizaco', 'Calle Xicotencalt #10, Apizaco, Tlaxcala', '244-434-5345', 'info@angeles.com');


/*INSERCION DE AREA O ESPECIALIDAD*/
INSERT INTO AREA (nombre_area, descripcion, id_laboratorio) 
VALUES ('Hematologia', 'Estudio y analisis para el tratamiento de enfermedades de la sangre ', '1');
INSERT INTO AREA (nombre_area,id_laboratorio, descripcion) VALUES ('Inmunologia',1,'Inmunologi'); 
INSERT INTO AREA (nombre_area,id_laboratorio, descripcion) VALUES ('Microbiologia',1,'Microbiologia'); 
INSERT INTO AREA (nombre_area,id_laboratorio, descripcion) VALUES ('Pruebas Especiales',1,'Pruebas Especiales');
INSERT INTO AREA (nombre_area,id_laboratorio, descripcion) VALUES ('Parasitologia',1,'Parasitologia');


INSERT ALL INTO ROL (nombre_rol) VALUES ('PACIENTE') 
           INTO ROL (nombre_rol) VALUES ('ESPECIALISTA')
           INTO ROL (nombre_rol) VALUES ('TECNICO ESPECIALISTA')
           INTO ROL (nombre_rol) VALUES ('ENFERMERA')
           INTO ROL (nombre_rol) VALUES ('VIGILANCIA')
           SELECT * FROM DUAL;

INSERT ALL INTO tipo_muestra (nombre_tipo) VALUES ('SANGUINEO')
       INTO tipo_muestra (nombre_tipo) VALUES ('HECES')
       INTO tipo_muestra (nombre_tipo) VALUES ('ORINA')
       SELECT * FROM DUAL;

INSERT ALL INTO analisis (nombre_analisis, id_tipo_muestra) VALUES ('Prueba ABO', 1)
INTO analisis (nombre_analisis, id_tipo_muestra) VALUES ('Prueba RH', 1)
INTO analisis (nombre_analisis, id_tipo_muestra) VALUES ('Anticuerpos Irregulares', 2)
INTO analisis (nombre_analisis, id_tipo_muestra) VALUES ('Prueba Pruebas Cruzadas', 3)
SELECT * FROM DUAL;


INSERT ALL INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (1,1) 
        INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (21,1)
        INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (3,2)
        INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (4,3)
        INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (2,4)
        INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (22,4)
        SELECT * FROM DUAL;



INSERT INTO SERVICIOS (nombre_servicio, descripcion, indicaciones, costo, tiempo_estimado, id_area)
VALUES ('Compatibilidad Sanguinea', 'Prueba previa al momento de donar sangre entre pacientes',
        'No ingerir alimentos almenos 7 horas previas a la toma de la muestra', 450.50, 20, 1);



INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Inmunoglobulina A (IgA)','
presente en grandes concentraciones en las membranas mucosas, particularmente
en las paredes internas de las vias respiratorias y el tracto gastrointestinal.',350,'El medico le dira si es necesario realizar una preparacion 
especial antes del analisis.',10,2);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Inmunoglobulina G (IgG)','
el tipo de anticuerpo mas abundante en los liquidos corporales. Brinda proteccion contra las bacterias y las infecciones virales.',350,'El medico le dira si es necesario realizar una preparacion 
especial antes del analisis.',10,2);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Inmunoglobulina M (IgM)','
se encuentra principalmente en la sangre y en el líquido linfatico. Es el primer anticuerpo que el cuerpo genera para combatir una infeccion.',350,'El medico le dira si es necesario realizar una preparacion 
especial antes del analisis.',10,2);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Inmunoglobulina E (IgE)','
se la asocia principalmente con las reacciones alergicas (lo que ocurre cuando el sistema inmunologico reacciona de manera exagerada a los antígenos del medio ambiente, como el polen o el polvillo de los animales).',350,'El medico le dira si es necesario realizar una preparacion 
especial antes del analisis.',10,2);





INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Acido urico en la sangre','
Esta prueba mide la cantidad de acido urico en una muestra de sangre. El acido urico es producto de la descomposicion natural de las células de su cuerpo y de los alimentos que ingiere.',430,'El medico le dira si es necesario realizar una preparacion 
especial antes del analisis.',30,3);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Dioxido de carbono','
Esta prueba mide el nivel de bicarbonato en una muestra de sangre de una vena. El bicarbonato es una sustancia química que actúa como un amortiguador. Evita que el pH de la sangre se vuelva demasiado acido o demasiado basico.',200,'El medico le dira si es necesario realizar una preparaci�n 
especial antes del analisis.',15,3);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Nitrogeno ureico en sangre','
El anAlisis de nitrogeno ureico en sangre (BUN, por sus siglas en ingles) mide la cantidad de nitrogeno en la sangre que proviene de un producto de desecho, llamado urea. ',350,'El medico le dira si es necesario realizar una preparacion 
especial antes del analisis.',30,3);

