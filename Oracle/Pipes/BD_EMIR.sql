set serveroutput on;
declare
   v number;
   m varchar2(4096);
begin
-- Wait 3 seconds...
   dbms_lock.sleep(3);
-- ... then write a message into a pipe
    -- dbms_pipe.pack_message('Hello from the blue session!');
   dbms_pipe.pack_message('&Registrar');
   v := dbms_pipe.send_message('tq84-send-receive-pipe');

   dbms_output.put_line('Message sent [' || v || ']');

    v := dbms_pipe.receive_message('tq84-send-receive-pipe');
    dbms_pipe.unpack_message(m);
    dbms_output.put_line('Message received [' || v || ']');
    dbms_output.put_line('  ' || m);
end;
/

exit



CREATE OR REPLACE PROCEDURE proc_pipe_002(r VARCHAR2)
IS
    v number;
BEGIN  
-- Wait 3 seconds...
   
-- ... then write a message into a pipe
    -- dbms_pipe.pack_message('Hello from the blue session!');
    dbms_lock.sleep(3);
    dbms_pipe.pack_message('&r');
    v := dbms_pipe.send_message('tq84-send-receive-pipe');
    dbms_output.put_line('Message sent [' || v || ']');
    
     v := dbms_pipe.receive_message('tq84-send-receive-pipe');
     dbms_pipe.unpack_message(m);
    dbms_output.put_line('Message received [' || v || ']');
    dbms_output.put_line('  ' || m);
    
END;
/

BEGIN
PROC_PIPE_002('&n');
END;
/
EXIT