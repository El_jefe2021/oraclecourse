set serveroutput on;
declare

   v number;

   m varchar2(4096);
 
   n number;
begin

-- Waiting for the message sent in the green session:
   
   v := dbms_pipe.receive_message('tq84-send-receive-pipe');

   dbms_pipe.unpack_message(m);
    
   IF(m = 'si') THEN
      dbms_lock.sleep(3);
      dbms_pipe.pack_message('&r');
      v := dbms_pipe.send_message('tq84-send-receive-pipe');
   END IF;
   dbms_output.put_line('Message received [' || v || ']');
   dbms_output.put_line('  ' || m);
  
    

end;
/

exit