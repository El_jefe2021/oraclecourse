/* Filename on companion disk: pipex1.sql */
DECLARE 
/* || PL/SQL block illustrating use of || 
DBMS_PIPE.PACK_MESSAGE to pack and send || 
a PL/SQL record to a pipe */ 

TYPE friend_rectype IS RECORD (name VARCHAR2(60) ,birthdate DATE ,weight_lbs NUMBER ); 
friend_rec friend_rectype; 
PROCEDURE pack_send_friend (friend_rec_IN IN friend_rectype ,pipename_IN IN VARCHAR2) 
IS 
call_status INTEGER; 
BEGIN 
/* ||notice the PACK_MESSAGE overloading */ 

DBMS_PIPE.PACK_MESSAGE(friend_rec_IN.name); 
DBMS_PIPE.PACK_MESSAGE(friend_rec_IN.birthdate); 
DBMS_PIPE.PACK_MESSAGE(friend_rec_IN.weight_lbs); 
call_status := DBMS_PIPE.SEND_MESSAGE (pipename=>pipename_IN,timeout=>0); 
IF call_status != 0 THEN 
    DBMS_OUTPUT.PUT_LINE('Send message failed'); 
END IF; 
END 
pack_send_friend; 
BEGIN /* || OK, now use the procedure to send a friend_rec */ 
friend_rec.name := 'John Smith'; 
friend_rec.birthdate := TO_DATE('01/14/55','MM/DD/YY'); 
friend_rec.weight_lbs := 175; 
pack_send_friend(friend_rec,'OPBIP_TEST_PIPE'); 
END; 
