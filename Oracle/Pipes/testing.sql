--PROCEDURE DBMS_PIPE.PACK_MESSAGE_RAW (item IN RAW);

/* Filename on companion disk: pipex1.sql */
/*
DECLARE 
hex_data VARCHAR2(12):='FFEEDDCCBBAA'; 
raw_data RAW(6); 
call_status INTEGER; 
BEGIN 
*/
/* create some raw data */ 

--raw_data := HEXTORAW(hex_data); 
/* || pack and send raw data on pipe */ 
/*DBMS_PIPE.PACK_MESSAGE_RAW(raw_data); 
call_status := DBMS_PIPE.SEND_MESSAGE('OPBIP_TEST_PIPE'); 
IF call_status != 0 THEN 
DBMS_OUTPUT.PUT_LINE('Send message failed'); 
END IF; 
END;

PROCEDURE DBMS_PIPE.PACK_MESSAGE_ROWID (item IN ROWID);

*/

BEGIN
    execute pipe2('This is a long text message');
END;
