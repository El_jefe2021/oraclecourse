CREATE OR REPLACE PACKAGE dbms_stock_server IS
      PROCEDURE get_request(symbol OUT VARCHAR2);
      PROCEDURE return_price(errormsg IN VARCHAR2, price IN VARCHAR2);
    END;
  
    CREATE OR REPLACE PACKAGE BODY dbms_stock_server IS
      returnpipe    VARCHAR2(30);
  
      PROCEDURE returnerror(reason VARCHAR2) IS
        s INTEGER;
      BEGIN
        dbms_pipe.pack_message(reason);
        s := dbms_pipe.send_message(returnpipe);
        IF s <> 0 THEN
          raise_application_error(-20000, 'Error:' || to_char(s) ||
            ' sending on pipe');
        END IF;
      END;
  
      PROCEDURE get_request(symbol OUT VARCHAR2) IS
        protocol_version VARCHAR2(10);
        s                INTEGER;
        service          VARCHAR2(30);
      BEGIN
        s := dbms_pipe.receive_message('stock_service');
        IF s <> 0 THEN
          raise_application_error(-20000, 'Error:' || to_char(s) ||
            'reading pipe');
        END IF;
        dbms_pipe.unpack_message(protocol_version);
        IF protocol_version <> '1' THEN
          raise_application_error(-20000, 'Bad protocol: ' || 
            protocol_version);
        END IF;
        dbms_pipe.unpack_message(returnpipe);
        dbms_pipe.unpack_message(service);
        IF service != 'getprice' THEN
          returnerror('Service ' || service || ' not supported');
        END IF;
        dbms_pipe.unpack_message(symbol);
      END;
  
      PROCEDURE return_price(errormsg in VARCHAR2, price in VARCHAR2) IS
        s INTEGER;
      BEGIN
        IF errormsg is NULL THEN
          dbms_pipe.pack_message('SUCCESS');
          dbms_pipe.pack_message(price);
        ELSE
          dbms_pipe.pack_message(errormsg);
        END IF;
        s := dbms_pipe.send_message(returnpipe);
        IF s <> 0 THEN
          raise_application_error(-20000, 'Error:'||to_char(s)||
            ' sending on pipe');
        END IF;
      END;
    END;