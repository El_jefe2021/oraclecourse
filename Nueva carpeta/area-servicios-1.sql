alter table especialista add
(cedula varchar2(50),
rfc varchar2(10),
referencia1 varchar2(250),
referencia2 varchar2(250));

alter table personal_admin add
(cedula varchar2(250),
rfc varchar2(250));

select nombre_area from area;

--delete from area where id_area = 23;

INSERT INTO AREA (nombre_area,id_laboratorio, descripcion) VALUES ('Inmunología',1,'Inmunología'); 
INSERT INTO AREA (nombre_area,id_laboratorio, descripcion) VALUES ('Microbiología',1,'Microbiología'); 
INSERT INTO AREA (nombre_area,id_laboratorio, descripcion) VALUES ('Pruebas Especiales',1,'Pruebas Especiales');
INSERT INTO AREA (nombre_area,id_laboratorio, descripcion) VALUES ('Parasitología',1,'Parasitología');

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Inmunoglobulina A (IgA)','
presente en grandes concentraciones en las membranas mucosas, particularmente
en las paredes internas de las vias respiratorias y el tracto gastrointestinal.',350,'El médico le dirá si es necesario realizar una preparación 
especial antes del analisis.',10,24);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Inmunoglobulina G (IgG)','
el tipo de anticuerpo mas abundante en los liquidos corporales. Brinda protección contra las bacterias y las infecciones virales.',350,'El médico le dirá si es necesario realizar una preparación 
especial antes del analisis.',10,24);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Inmunoglobulina M (IgM)','
se encuentra principalmente en la sangre y en el liquido linfatico. Es el primer anticuerpo que el cuerpo genera para combatir una infección.',350,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',10,24);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Inmunoglobulina E (IgE)','
se la asocia principalmente con las reacciones alérgicas (lo que ocurre cuando el sistema inmunológico reacciona de manera exagerada a los antígenos del medio ambiente, como el polen o el polvillo de los animales).',350,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',10,24);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('�?cido úrico en la sangre','
Esta prueba mide la cantidad de ácido úrico en una muestra de sangre. El ácido úrico es producto de la descomposición natural de las células de su cuerpo y de los alimentos que ingiere.',430,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',30,2);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Dióxido de carbono','
Esta prueba mide el nivel de bicarbonato en una muestra de sangre de una vena. El bicarbonato es una sustancia química que actúa como un amortiguador. Evita que el pH de la sangre se vuelva demasiado ácido o demasiado básico.',200,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',15,2);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Nitrógeno ureico en sangre','
El análisis de nitrógeno ureico en sangre (BUN, por sus siglas en inglés) mide la cantidad de nitrógeno en la sangre que proviene de un producto de desecho, llamado urea. ',350,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',30,2);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Cultivo de orina','
Se usa para diagnosticar una infección del tracto urinario e identificar la bacteria causante de la infección
',220,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',30,25);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Cultivo de heces','
Se usa para detectar infecciones causadas por bacterias o parásitos del aparato digestivo, por ejemplo, intoxicación alimentaria y otras enfermedades digestivas',500,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',45,25);


INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Cultivo de una herida','
Se usa para detectar infecciones en heridas abiertas o en lesiones por quemaduras',200,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',45,25);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Hemograma','
El hemograma es una herramienta crítica para la evaluación clínica de la hematopoyesis.',200,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',45,3);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Endoscopia/Colonoscopia','
La endoscopia se usa para detectar parásitos que provocan diarrea, heces blandas o líquidas, cólicos, flatulencias (gases) y otras enfermedades abdominales.',600,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',50,27);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Resonancia magnética (RM)','
Estas pruebas se usan para buscar algunas enfermedades parasitarias que pueden provocar lesiones en los órganos.',450,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',35,27);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Bypass gástrico (en Y de Roux)','
El baipás gástrico, también denominado "baipás gástrico en Y de Roux", es un tipo de cirugía para bajar de peso que consiste en crear una pequeña bolsa desde el estómago y conectar la bolsa 
recién creada directamente con el intestino delgado.',800,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',10,26);

INSERT INTO SERVICIOS(nombre_servicio,descripcion,costo,indicaciones,tiempo_estimado,id_area) VALUES('Biopsia','
Se realizan por el método de aspiración, es decir, pinchando con una guja y succionar para obtener una muestra de tejido. Se realizan cuando se detectan nódulos en las glándulas tiroides o 
suprarrenales.',800,'El médico le dirá si es necesario realizar una preparación 
especial antes del análisis.',10,26);
