create or replace FUNCTION INSERT_PACIENTE(p_nombre varchar2,
                                            p_apellido_paterno varchar2,
                                            p_apellido_materno varchar2,
                                            p_edad number,
                                            p_peso number,
                                            p_estatura number,
                                            p_tipo_sangre varchar2,                                            
                                            p_username varchar2,
                                            p_pass varchar2,
                                            p_sexo varchar2,
                                            p_correo varchar2,
                                            p_telefono varchar2
                                            )
                                            RETURN VARCHAR2
IS

new_user NUMBER := 0;
BEGIN


    INSERT INTO USUARIO(username,pass,id_rol) 
        VALUES(p_username,p_pass,1)
        RETURNING id_usuario INTO new_user;

     

    INSERT INTO PACIENTE(nombre_paciente, apellido_paterno, apellido_materno, 
                         edad, peso, estatura, tipo_sangre, id_usuario, sexo, 
                         correo, telefono)
        VALUES(p_nombre, p_apellido_paterno, p_apellido_materno, p_edad,p_peso,
               p_estatura, p_tipo_sangre, new_user, p_sexo, p_correo, p_telefono);
               
RETURN 'PACIENTE INSERTADO';
END;
/




CREATE OR REPLACE PROCEDURE insertar_paciente (
    p_nombre            VARCHAR2,
    p_apellido_paterno  VARCHAR2,
    p_apellido_materno  VARCHAR2,
    p_edad              NUMBER,
    p_peso              NUMBER,
    p_sexo              VARCHAR2,
    p_correo            VARCHAR2,
    p_telefono          VARCHAR2,
    p_estatura          NUMBER,
    p_tipo_sangre       VARCHAR2,
    p_username          VARCHAR2,
    p_pass              VARCHAR2
                                            --p_id_usuario number(10)
) IS

--DECLARE 
    new_user NUMBER;
BEGIN
    INSERT INTO usuario (
        username,
        pass,
        id_rol
    ) VALUES (
        p_username,
        p_pass,
        1
    );

    
    SELECT
        MAX(id_usuario)
    INTO new_user
    FROM
        usuario;

    INSERT INTO paciente (
        nombre_paciente,
        apellido_paterno,
        apellido_materno,
        edad,
        peso,
        estatura,
        sexo,
        telefono,
        correo,
        tipo_sangre,
        id_usuario
    ) VALUES (
        p_nombre,
        p_apellido_paterno,
        p_apellido_materno,
        p_edad,
        p_peso,
        p_estatura,
        p_sexo,
        p_telefono,
        p_correo,
        p_tipo_sangre,
        new_user
    );

END insertar_paciente;
/





DECLARE
    --new_user NUMBER := 0;
    resultado varchar2(100);
BEGIN
    --SELECT MAX(id_usuario)into new_user FROM USUARIO;
    resultado := INSERT_PACIENTE(:p_nombre, :p_apellido_paterno, :p_apellido_materno, 
              :p_edad, :p_peso, :p_estatura, :p_tipo_sangre, :p_username,
              :p_pass, :p_sexo, :p_correo, :p_telefono);
    dbms_output.put_line(resultado);
END;
/