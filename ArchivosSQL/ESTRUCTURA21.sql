
-- INSERT INTO RESULTADO (resultado_id, nombre, resultado) VALUES (1,'EMIR', 75);


CREATE OR REPLACE TRIGGER trig_res_001
BEFORE INSERT ON RESULTADO 
FOR EACH ROW
BEGIN
  SELECT rest_seq.NEXTVAL
  INTO   :new.resultado_id
  FROM   dual;
  
  SELECT GET_RES_010(new.resultado) INTO new.resultado FROM DUAL;
  
  SELECT EXMEN_011(new.resultado) INTO new.mensaje FROM DUAL;
  
END;
/

/* 
CREATE TABLE resultado(
    resultado_id NUMBER(9),
    nombre VARCHAR2(100),
    resultado NUMBER(4),
    mensaje VARCHAR2(200),
    fecha_resultado DATE DEFAULT SYSDATE,
    CONSTRAINT resultados_pk PRIMARY KEY(resultado_id)
     );
*/


-- CREATE SEQUENCE rest_seq START WITH 1;