SET SERVEROUTPUT ON
/*
CREATE OR REPLACE FUNCTION GET_RES_010(res NUMBER)
RETURN NUMBER
AS
    new_res NUMBER;
BEGIN
    IF res>= 66 AND res <= 69 THEN
        new_res := 70;
    ELSE 
        new_res := res;
    END IF;
    
    RETURN new_res;    
END;
/
*/
/*
CREATE OR REPLACE FUNCTION EXMEN_011(l_res NUMBER)
RETURN VARCHAR2
AS
    l_f_res NUMBER;
    l_message NVARCHAR2(100);
    l_message2 NVARCHAR2(100);
BEGIN
    
    l_f_res := GET_RES_010(l_res);

    IF l_f_res <= 65 then
        l_message := '- No acreditado';
        l_message2 := ' Suerte para la proxima';
    ELSE
        IF l_f_res >= 70 AND l_f_res <= 80 THEN
            l_message2 := ' Bien';            
        ELSIF l_f_res >= 81 AND l_f_res <= 90 THEN
            l_message2 := ' Muy Bien';
        ELSIF l_f_res >= 91 THEN
            l_message2 := ' Exelente';        
        END IF;
    l_message := '- Acreditado';        
    END IF;
    RETURN 'Resultado: ' || l_F_res || l_message2 || ' ' || l_message;
 END;
 /
*/
 /*
CREATE TABLE resultado(
    resultado_id NUMBER(9),
    nombre VARCHAR2(100),
    resultado NUMBER(4),
    mensaje VARCHAR2(200),
    fecha_resultado DATE DEFAULT SYSDATE,
    CONSTRAINT resultados_pk PRIMARY KEY(resultado_id)
     );

CREATE SEQUENCE rest_seq START WITH 1;

*/

CREATE OR REPLACE TRIGGER trig_res_001
BEFORE INSERT ON RESULTADO 
FOR EACH ROW
BEGIN
  SELECT rest_seq.NEXTVAL
  INTO   :new.resultado_id
  FROM   dual;
  
  SELECT GET_RES_010(new.resultado) INTO new.resultado FROM DUAL;
  
  SELECT EXMEN_011(new.resultado) INTO new.mensaje FROM DUAL;
  
END;
/


-- INSERT INTO RESULTADO (resultado_id, nombre, resultado) VALUES (1,'EMIR', 75);


