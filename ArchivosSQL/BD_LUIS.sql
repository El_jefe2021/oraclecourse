SET SERVEROUTPUT ON
/*CREATE OR REPLACE FUNCTION GET_RES_010(res NUMBER)
RETURN NUMBER
AS
    new_res NUMBER;
BEGIN
    IF res>= 66 AND res <= 69 THEN
        new_res := 70;
    ELSE 
        new_res := res;
    END IF;
    
    RETURN new_res;    
END;
/


CREATE OR REPLACE FUNCTION EXMEN_011(l_res NUMBER)
RETURN VARCHAR2
AS
    l_f_res NUMBER;
    l_message NVARCHAR2(100);
    l_message2 NVARCHAR2(100);
BEGIN
    
    l_f_res := GET_RES_010(l_res);

    IF l_f_res <= 65 then
        l_message := '- No acreditado';
        l_message2 := ' Suerte para la proxima';
    ELSE
        IF l_f_res >= 70 AND l_f_res <= 80 THEN
            l_message2 := ' Bien';            
        ELSIF l_f_res >= 81 AND l_f_res <= 90 THEN
            l_message2 := ' Muy Bien';
        ELSIF l_f_res >= 91 THEN
            l_message2 := ' Exelente';        
        END IF;
    l_message := '- Acreditado';        
    END IF;
    RETURN 'Resultado: ' || l_F_res || l_message2 || ' ' || l_message;
 END;
 /
*/

/*
CREATE TABLE result_001(
    res_id NUMBER(9),
    nombre VARCHAR2(100),
    res NUMBER(4),
    mensaje VARCHAR2(200),
    fecha DATE DEFAULT SYSDATE,
    CONSTRAINT result_pk PRIMARY KEY(res_id)
     );
*/

--  CREATE SEQUENCE rest_seq_01 START WITH 1;


CREATE OR REPLACE TRIGGER trig_res_001
BEFORE INSERT ON result_001 
FOR EACH ROW
BEGIN
 SELECT rest_seq_01.NEXTVAL
  INTO   new.RES_id
  FROM   dual;
  
  SELECT GET_RES_010(new.RES) INTO new.res FROM result_001;
  
  SELECT EXMEN_011(:new.RES) INTO new.mensaje FROM result_001;
  
END;
/


-- INSERT INTO RESULTADO (resultado_id, nombre, resultado) VALUES (1,'EMIR', 75);




/*
 create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );

*/


-- select privilege from user_sys_privs; -- Obtenemos la siguiente información:PRIVILEGE