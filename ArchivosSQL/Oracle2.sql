SET SERVEROUTPUT ON
DECLARE
FUNCTION GET_RES_01(res NUMBER)
RETURN NUMBER
AS
    new_res NUMBER;
BEGIN
    IF res>= 66 AND res <= 69 THEN
        new_res := 70;
    ELSE 
        new_res := res;
    END IF;
    
    RETURN new_res;    
END GET_RES_01;
BEGIN
       EXAMEN_03(GET_RES_01(66));
END;
/


