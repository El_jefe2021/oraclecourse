 alter session set "_ORACLE_SCRIPT"=true;  
    create user laboratorio identified by lab12345 -- emir123--luis123
    default tablespace system
    quota 100M on system;
 
    grant create session
    to LABORATORIO;
 
    grant create table, create view
    to LABORATORIO;
 
     grant create trigger, create procedure
    to LABORATORIO;
 
 
    grant create sequence
    to LABORATORIO;
 
    GRANT EXECUTE ON DBMS_LOCK TO LABORATORIO;
 
    GRANT EXECUTE ON DBMS_PIPE TO LABORATORIO;
 
    select grantee, privilege from dba_sys_privs
    where grantee='LABORATORIO' order by grantee;
    
    Declare
    ret_code number;

    begin
        ret_code := dbms_pipe.remove_pipe('QUEST_EXEC');
    end;
/

