/*CREATE TABLE EMPLEADOS(
    id NUMBER(11),
    nombre VARCHAR2(100),
    apellido_p VARCHAR2(100),
    apellido_m VARCHAR2(100),
    sueldo NUMBER(11),
    fecha_registro DATE DEFAULT SYSDATE,
    CONSTRAINT empleados_pk PRIMARY KEY(id)
     );
*/
--CREATE SEQUENCE emp_seq START WITH 1 INCREMENT BY 1;

-- ALTER TABLE EMPLEADOS ADD status NUMBER(1);

-- DESCRIBE EMPLEADOS;
/*
CREATE OR REPLACE TRIGGER trig_emp_001
BEFORE INSERT ON EMPLEADOS
FOR EACH ROW
BEGIN
  SELECT emp_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/
*/


/*INSERT INTO EMPLEADOS (nombre, apellido_p, apellido_m, sueldo, status) 
       VALUES ('Katherine', 'Pereyra', 'Zuleta', 9700, 1); 
*/  

-- INHABILITAR --  
-- UPDATE empleados SET status = 0 WHERE id = 1;
/*
CREATE OR REPLACE PROCEDURE proc_reg_emp_001(n VARCHAR2,
                                             a_p VARCHAR2,
                                             a_m VARCHAR2,
                                             s NUMBER)   
IS
BEGIN
    INSERT INTO EMPLEADOS (nombre, apellido_p, apellido_m, sueldo, status) 
       VALUES (n, a_p, a_m, s, 1);
    
END;
/
*/

/*
CREATE OR REPLACE PROCEDURE proc_reg_emp_002   
IS
BEGIN
    INSERT INTO EMPLEADOS (nombre, apellido_p, apellido_m, sueldo, status) 
       VALUES ('&nom', '&ap_p', '&ap_m', &sueldo, 1);
    
END;
/
*/
/*
BEGIN
proc_reg_emp_001('&nom', '&ap_p', '&ap_m', &sueldo); 
END;
/

SELECT * FROM EMPLEADOS;
*/

--describe empleados;


/*CREATE TABLE EMPLEADOS(
    id NUMBER(11),
    nombre VARCHAR2(100),
    apellido_p VARCHAR2(100),
    apellido_m VARCHAR2(100),
    sueldo NUMBER(11),
    fecha_registro DATE DEFAULT SYSDATE,
    CONSTRAINT empleados_pk PRIMARY KEY(id)
     );
*/
--CREATE SEQUENCE emp_seq START WITH 1 INCREMENT BY 1;

-- ALTER TABLE EMPLEADOS ADD status NUMBER(1);

-- DESCRIBE EMPLEADOS;
/*
CREATE OR REPLACE TRIGGER trig_emp_001
BEFORE INSERT ON EMPLEADOS
FOR EACH ROW
BEGIN
  SELECT emp_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/
*/


/*INSERT INTO EMPLEADOS (nombre, apellido_p, apellido_m, sueldo, status) 
       VALUES ('Katherine', 'Pereyra', 'Zuleta', 9700, 1); 
*/  

-- INHABILITAR --  
-- UPDATE empleados SET status = 0 WHERE id = 1;
/*
CREATE OR REPLACE PROCEDURE proc_reg_emp_001(n VARCHAR2,
                                             a_p VARCHAR2,
                                             a_m VARCHAR2,
                                             s NUMBER)   
IS
BEGIN
    INSERT INTO EMPLEADOS (nombre, apellido_p, apellido_m, sueldo, status) 
       VALUES (n, a_p, a_m, s, 1);
    
END;
/
*/

/*
CREATE OR REPLACE PROCEDURE proc_reg_emp_002   
IS
BEGIN
    INSERT INTO EMPLEADOS (nombre, apellido_p, apellido_m, sueldo, status) 
       VALUES ('&nom', '&ap_p', '&ap_m', &sueldo, 1);
    
END;
/
*/
/*
BEGIN
proc_reg_emp_001('&nom', '&ap_p', '&ap_m', &sueldo); 
END;
/

SELECT * FROM EMPLEADOS;
*/

--describe empleados;

/*
set serveroutput on;
DECLARE 
    a VARCHAR(2);
BEGIN    
    a:='&continuar';
    --WHILE a='si' OR a='SI' OR a = 's' LOOP
    -- PROC_REG_EMP_001('&nom', '&ap_p', '&ap_m', &sueldo);    
    IF (a='si' OR a='SI' OR a='s') THEN
        -- GOTO registro;       
        proc_reg_emp_001('&nom', '&ap_p', '&ap_m', &sueldo); 
        --a:= '&otro';
    --END LOOP;  
    ELSE
        dbms_output.put_line('No hay mas registros');   
    END IF; 
    
END;
/

*/


/*DECLARE 
    a VARCHAR(2);
BEGIN    
    a:='&continuar';
    --WHILE a='si' OR a='SI' OR a = 's' LOOP
    -- PROC_REG_EMP_001('&nom', '&ap_p', '&ap_m', &sueldo);    
    IF (a='si' OR a='SI' OR a='s') THEN
        -- GOTO registro;       
        loop 
        proc_reg_emp_001('&nom', '&ap_p', '&ap_m', &sueldo); 
        --a:= '&otro';
    --END LOOP;  
    ELSE
        dbms_output.put_line('No hay mas registros');   
    END IF; 
    
END;
/

*/


/*
DECLARE 
    a VARCHAR(2);
BEGIN    
    <<inicio>>
    a:=0;
    a:='&continuar';
    IF a='si' THEN
    --LOOP           
         
         proc_reg_emp_001('&nom', '&ap_p', '&ap_m', &sueldo); 
         
    --    dbms_output.put_line('No hay mas registros');   
    -- END LOOP;
    END IF;
    --GOTO inicio;
END;
/


SELECT * FROM EMPLEADOS;
*/


DECLARE 
    a number := null;
BEGIN
    
    LOOP
        a := &n;
    EXIT WHEN a=0;
        dbms_output.put_line(a); 
    END LOOP;
    --end loop;
END;
/