DECLARE
 nombre VARCHAR(100);
 operacion CHAR(1);
 longitud NUMBER;
 n1 NUMBER;
 n2 NUMBER;
 res NUMBER;
BEGIN


 nombre:= '&nombre';
 SELECT LENGTH(nombre) INTO longitud FROM DUAL;

    n1 := &n1;
    operacion := '&operacion';
    n2 := &n2;
    
    IF (n1 <= 0 OR n2 <= 0) THEN
        dbms_output.put_line('No se aceptan negativos');   
    ElSE
    CASE operacion
        WHEN '+' THEN
            res := n1+n2;
        WHEN '-' THEN
            res := n1- n2;
        WHEN '*' THEN
            res := n1 * n2;
        WHEN '/' THEN
            res := n1/n2;    
    END CASE;
    
        dbms_output.put_line(nombre || ' Tiene ' || longitud || ' Caracteres');
        dbms_output.put_line('Resultado de la operación ' || n1 || operacion || n2 || ' = '|| res);
        dbms_output.put_line('Potencia ' || res || ' ^ ' || longitud || ' = ' || res**longitud);
    END IF;
 
EXCEPTION
    WHEN VALUE_ERROR THEN  
        dbms_output.put_line('Exede la longitud del operador');   
    WHEN CASE_NOT_FOUND THEN
        dbms_output.put_line('No se reconoce el simbolo para la operación');   
END;
/