/***
Base de datos para Laboratorio Clinico
Version 0.0.1
Autor: Luis Emir Luna Minor
Coautor: Brenda
***/


--Tabla Laboratorio--

CREATE TABLE LABORATORIO(
    id_laboratorio NUMBER(11),
    nombre_laboratorio NVARCHAR2(100),
    direccion NVARCHAR2(250),
    telefono NVARCHAR2(20),
    correo NVARCHAR2(150),
    CONSTRAINT lab_id_pk PRIMARY KEY(id_laboratorio)
);

CREATE SEQUENCE lab_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_001
BEFORE INSERT ON LABORATORIO
FOR EACH ROW
BEGIN
  SELECT lab_seq.NEXTVAL
  INTO   :new.id_laboratorio
  FROM   dual;
END;
/



--Tabla Area--

CREATE TABLE AREA(
    id_area NUMBER(5),
    nombre_area NVARCHAR2(250),
    descripcion NVARCHAR2(250),
    id_laboratorio NUMBER(11),
    CONSTRAINT area_id_pk PRIMARY KEY(id_area),
    CONSTRAINT fk_laboratorio
    FOREIGN KEY (id_laboratorio)
    REFERENCES LABORATORIO(id_laboratorio)
);

CREATE SEQUENCE area_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_002
BEFORE INSERT ON AREA
FOR EACH ROW
BEGIN
  SELECT area_seq.NEXTVAL
  INTO   :new.id_area
  FROM   dual;
END;
/



--Tabla Servicio--

CREATE TABLE SERVICIO(
    id_servicio NUMBER(5),
    nombre_servicio NVARCHAR2(250),
    descripcion NVARCHAR2(250),
    indicaciones NVARCHAR2(250),
    costo DECIMAL(10,2),
    tiempo_estimado NUMBER(10), -- MINUTOS    
    id_area NUMBER(5),
    CONSTRAINT servicio_id_pk PRIMARY KEY(id_servicio),
    CONSTRAINT fk_area
    FOREIGN KEY (id_area)
    REFERENCES AREA(id_area)

);

CREATE SEQUENCE servicio_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_003
BEFORE INSERT ON SERVICIO
FOR EACH ROW
BEGIN
  SELECT servicio_seq.NEXTVAL
  INTO   :new.id_servicio
  FROM   dual;
END;
/


--Tabla Tipo de muestra--

CREATE TABLE TIPO_MUESTRA(
    id_tipo_muestra NUMBER(4),
    nombre_tipo NVARCHAR2(250),
    CONSTRAINT tipo_muestra_id_pk PRIMARY KEY(id_tipo_muestra)    
);

CREATE SEQUENCE tipo_muestra_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_004
BEFORE INSERT ON TIPO_MUESTRA
FOR EACH ROW
BEGIN
  SELECT tipo_muestra_seq.NEXTVAL
  INTO   :new.id_tipo_muestra
  FROM   dual;
END;
/


--Tabla Analisis--

CREATE TABLE ANALISIS(
    id_analisis NUMBER(4),
    nombre_analisis NVARCHAR2(250),
    id_tipo_muestra number(4),
    CONSTRAINT analisis_id_pk PRIMARY KEY(id_analisis),
    CONSTRAINT fk_muestra
    FOREIGN KEY (id_tipo_muestra)
    REFERENCES TIPO_MUESTRA(id_tipo_muestra)
);

CREATE SEQUENCE analisis_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_005
BEFORE INSERT ON ANALISIS
FOR EACH ROW
BEGIN
  SELECT analisis_seq.NEXTVAL
  INTO   :new.id_analisis
  FROM   dual;
END;
/



CREATE TABLE SERV_ANALISIS(
    id_servicio NUMBER(11),
    id_analisis NUMBER (11),
    CONSTRAINT fk_serv_analisis FOREIGN KEY (id_servicio) REFERENCES SERVICIO(id_servicio),
    CONSTRAINT fk_analisis_serv FOREIGN KEY (id_analisis) REFERENCES ANALISIS(id_analisis)
);
/



-- Tabla Usuario--
CREATE TABLE USUARIO(
    id_usuario number(10),
    username varchar2(20),
    pass varchar2(20),
    id_rol number(10),
    CONSTRAINT pk_usuario PRIMARY KEY (id_usuario),
    CONSTRAINT fk_rol_u FOREIGN KEY (id_rol) REFERENCES rol(id_rol)
);

CREATE SEQUENCE usuario_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_006
BEFORE INSERT ON USUARIO
FOR EACH ROW
BEGIN
  SELECT usuario_seq.NEXTVAL
  INTO   :new.id_usuario
  FROM   dual;
END;
/



--Tabla Paciente--

CREATE TABLE PACIENTE(
    id_paciente NUMBER(15),
    nombre_paciente NVARCHAR2(150),
    apellido_paterno NVARCHAR2(150),
    apellido_materno NVARCHAR2(150),
    edad NUMBER(3),
    peso DECIMAL(5,2),
    estatura DECIMAL(5,2),
    tipo_sangre NVARCHAR2(10),
    id_usuario NUMBER(11),
    CONSTRAINT paciente_id_pk PRIMARY KEY(id_paciente),
    CONSTRAINT fk_usuario FOREIGN KEY (id_usuario) REFERENCES USUARIO(id_usuario)
);

CREATE SEQUENCE paciente_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_007
BEFORE INSERT ON PACIENTE
FOR EACH ROW
BEGIN
  SELECT paciente_seq.NEXTVAL
  INTO   :new.id_paciente
  FROM   dual;
END;
/

--Tabla Pago--
CREATE TABLE PAGO(
    id_pago NUMBER(15),
    monto DECIMAL(10,2),
    metodo_pago NUMBER(2),
    fecha DATE,
    CONSTRAINT pago_id_pk PRIMARY KEY(id_pago)    
);

CREATE SEQUENCE pago_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_008
BEFORE INSERT ON PAGO
FOR EACH ROW
BEGIN
  SELECT pago_seq.NEXTVAL
  INTO   :new.id_pago
  FROM   dual;
END;
/




--  Tabla Perfil --
CREATE TABLE PERFIL(
    id_perfil number(10),
    nombre varchar2(50),
    apellidoP varchar2(50),
    apellidoM varchar2(50),
    edad number(3),
    sexo varchar2(2),
    turno varchar2(20),
    CONSTRAINT perfil_pk PRIMARY KEY (id_perfil)
);

ALTER TABLE PERFIL ADD TELEFONO VARCHAR2(20);
ALTER TABLE PERFIL ADD CORREO VARCHAR2(100);

CREATE SEQUENCE pefil_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_009
BEFORE INSERT ON PERFIL
FOR EACH ROW
BEGIN
  SELECT perfil_seq.NEXTVAL
  INTO   :new.id_perfil
  FROM   dual;
END;
/





-- Tabla Roles --
CREATE TABLE ROL(
    id_rol number(3),
    nombre_rol varchar2(20),
    CONSTRAINT rol_pk PRIMARY KEY (id_rol)
);

CREATE SEQUENCE rol_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_010
BEFORE INSERT ON ROL
FOR EACH ROW
BEGIN
  SELECT rol_seq.NEXTVAL
  INTO   :new.id_rol
  FROM   dual;
END;
/





-- Tabla Personal No Facultativo --
CREATE TABLE PERSONAL_NO_FACULTATIVO(
    id_personal_nf number(10),
    id_perfil number(10),
    sueldo number(6,2),
    id_rol number(10),
    CONSTRAINT personal_no_facultativo_pk PRIMARY KEY (id_personal_nf)
);

ALTER TABLE PERSONAL_NO_FACULTATIVO ADD CONSTRAINT perfil_pnf_fk 
FOREIGN KEY (id_perfil) REFERENCES PERFIL(id_perfil);

CREATE SEQUENCE personal_nf_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_011
BEFORE INSERT ON PERSONAL_NO_FACULTATIVO
FOR EACH ROW
BEGIN
  SELECT personal_nf_seq.NEXTVAL
  INTO   :new.id_personal_nf
  FROM   dual;
END;
/


-- Tabla Tecnico Especialista--
CREATE TABLE TECNICO_ESPECIALISTA(
    id_tecnico_especialista number(10),
    id_usuario number(10),
    id_perfil number(10),
    CONSTRAINT tecnico_especialista_pk PRIMARY KEY (id_tecnico_especialista),
    CONSTRAINT fk_usuario_t FOREIGN KEY (id_usuario) REFERENCES USUARIO(id_usuario),
    CONSTRAINT fk_perfil_t FOREIGN KEY (id_perfil) REFERENCES PERFIL(id_perfil)
);

CREATE SEQUENCE tec_esp_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_018
BEFORE INSERT ON TECNICO_ESPECIALISTA
FOR EACH ROW
BEGIN
  SELECT tec_esp_seq.NEXTVAL
  INTO   :new.id_tecnico_especialista
  FROM   dual;
END;
/





--Tabla Personal admin --
CREATE TABLE PERSONAL_ADMIN(
    id_personal_admin number(10),
    id_perfil number(10),
    id_usuario number(10),
    CONSTRAINT personal_admin_pk PRIMARY KEY (id_personal_admin),
    CONSTRAINT fk_usuario_pa FOREIGN KEY (id_usuario) REFERENCES USUARIO(id_usuario),
    CONSTRAINT fk_perfil_pa FOREIGN KEY (id_perfil) REFERENCES PERFIL(id_perfil)
    
);

CREATE SEQUENCE personal_ad_seq START WITH 1;


CREATE OR REPLACE TRIGGER trig_lab_012
BEFORE INSERT ON PERSONAL_ADMIN
FOR EACH ROW
BEGIN
  SELECT personal_ad_seq.NEXTVAL
  INTO   :new.id_personal_admin
  FROM   dual;
END;
/





--Tabla Solicitud--

CREATE TABLE SOLICITUD(
    id_solicitud NUMBER(15),
    id_paciente NUMBER(10),
    id_personal_admin NUMBER(5), 
    fecha_solicitud DATE,
    total_pagar DECIMAL(15,2),
    status NUMBER(1),
    id_pago NUMBER(15),
    CONSTRAINT solicitud_id_pk PRIMARY KEY(id_solicitud),
    CONSTRAINT fk_paciente_s FOREIGN KEY (id_paciente) REFERENCES PACIENTE(id_paciente),
    CONSTRAINT fk_personal_admin_s FOREIGN KEY (id_personal_admin) REFERENCES PERSONAL_ADMIN(id_personal_admin),
    CONSTRAINT fk_pago_S FOREIGN KEY (id_pago) REFERENCES PAGO(id_pago)
);

CREATE SEQUENCE solicitud_seq START WITH 1;


CREATE OR REPLACE TRIGGER trig_lab_013
BEFORE INSERT ON SOLICITUD
FOR EACH ROW
BEGIN
  SELECT solicitud_seq.NEXTVAL
  INTO   :new.id_solicitud
  FROM   dual;
END;
/

-- Tabla Especialista --
 CREATE TABLE ESPECIALISTA(
    id_especialista number(10),
    id_usuario number(10),
    id_perfil number(10),
    CONSTRAINT especialista_pk PRIMARY KEY (id_especialista),
    CONSTRAINT fk_usuario_e FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario),
    CONSTRAINT fk_perfil_e FOREIGN KEY (id_perfil) REFERENCES perfil(id_perfil)    
);

CREATE SEQUENCE especialista_seq START WITH 1;
 
CREATE OR REPLACE TRIGGER trig_lab_014
BEFORE INSERT ON ESPECIALISTA
FOR EACH ROW
BEGIN
  SELECT especialista_seq.NEXTVAL
  INTO   :new.id_especialista
  FROM   dual;
END;
/

 
 
--Tabla Servicio Especialidad --
CREATE TABLE servicio_especialista(
    id_servicio_especialista number(10),
    id_servicio number(10),
    id_especialista number(10),
     CONSTRAINT servicio_especialista_pk PRIMARY KEY (id_servicio_especialista),
    CONSTRAINT fk_servicio FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio),
    CONSTRAINT fk_especialista FOREIGN KEY (id_especialista) REFERENCES especialista(id_especialista)
    
);

CREATE SEQUENCE servicio_especialista_seq START WITH 1;
 
CREATE OR REPLACE TRIGGER trig_lab_015
BEFORE INSERT ON servicio_especialista
FOR EACH ROW
BEGIN
  SELECT servicio_especialista_seq.NEXTVAL
  INTO   :new.id_servicio_especialista
  FROM   dual;
END;
/




--Tabla Servicio Especialista Solicitud--
CREATE TABLE SERV_ESP_SOL(
    id_serv_esp_sol NUMBER(25),    
    id_solicitud NUMBER(15),
    id_serv_esp NUMBER(10),    
    fecha_serv_sol DATE,    
    CONSTRAINT serv_esp_sol_id_pk PRIMARY KEY(id_serv_esp_sol),
    CONSTRAINT fk_solicitud FOREIGN KEY (id_solicitud) REFERENCES SOLICITUD(id_solicitud),
    CONSTRAINT fk_serv_esp FOREIGN KEY (id_serv_esp) REFERENCES SERVICIO_ESPECIALISTA(id_servicio_especialista)
);

CREATE SEQUENCE serv_esp_sol_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_016
BEFORE INSERT ON SERV_ESP_SOL
FOR EACH ROW
BEGIN
  SELECT serv_esp_sol_seq.NEXTVAL
  INTO   :new.id_serv_esp_sol
  FROM   dual;
END;
/


--Tabla Muestreo--
CREATE TABLE MUESTREO(
    id_muestreo NUMBER(30),
    id_serv_esp_sol NUMBER(25),
    fecha_hora_obtencion_muestra DATE,
    fecha_hora_entrega_muestra DATE,
    observaciones NVARCHAR2(250),
    CONSTRAINT muestreo_id_pk PRIMARY KEY(id_muestreo),
    CONSTRAINT fk_serv_esp_sol FOREIGN KEY (id_serv_esp_sol) 
    REFERENCES SERV_ESP_SOL(id_serv_esp_sol)
);

CREATE SEQUENCE muestra_seq START WITH 1;

CREATE OR REPLACE TRIGGER trig_lab_017
BEFORE INSERT ON MUESTREO
FOR EACH ROW
BEGIN
  SELECT muestra_seq.NEXTVAL
  INTO   :new.id_muestreo
  FROM   dual;
END;
/




