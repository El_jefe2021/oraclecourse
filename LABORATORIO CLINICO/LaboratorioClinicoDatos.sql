/*INSERCION DE LABORATORIO*/
INSERT INTO LABORATORIO (nombre_laboratorio, direccion, telefono, correo) 
VALUES ('Angeles Apizaco', 'Calle Xicotencalt #10, Apizaco, Tlaxcala', '244-434-5345', 'info@angeles.com');

/*INSERCION DE AREA O ESPECIALIDAD*/
INSERT INTO AREA (nombre_area, descripcion, id_laboratorio) 
VALUES ('Hematologia', 'Estudio y analisis para el tratamiento de enfermedades de la sangre ', '1');

/*INSERCION DE SERVICIOS QUE OFRECE EL LABORATORIO*/
INSERT ALL INTO SERVICIO (nombre_servicio, descripcion, indicaciones, costo, tiempo_estimado, id_area)
VALUES ('Compatibilidad Sanguinea', 'Prueba previa al momento de donar sangre entre pacientes',
        'No ingerir alimentos almenos 7 horas previas a la toma de la muestra', 450.50, 20, 1)        
INTO SERVICIO(nombre_servicio, descripcion, indicaciones, costo, tiempo_estimado, id_area)  
VALUES('Morfologia', 'diagnóstico de muchas enfermedades hematológicas puede realizarse 
        con solo observar las características morfológicas de las células sanguíneas',
       'Este es un examen sencillo, poco costoso y rápido, pero que requiere de mucho cuidado y experiencia',
       350.50, 20, 1) SELECT * FROM DUAL;
       
/*INSERCION DE PERFILES*/
INSERT ALL INTO PERFIL (NOMBRE, APELLIDOP, APELLIDOM, EDAD, SEXO, TURNO, TELEFONO, CORREO) 
VALUES('JOSE', 'ROJAS', 'PASCUAL', 57, 'M', 'Matutino', '248-173-3833', 'pascualrojas@gmail.com')
INTO PERFIL (NOMBRE, APELLIDOP, APELLIDOM, EDAD, SEXO, TURNO, TELEFONO, CORREO)
VALUES('CARLOS', 'FLORES', 'PERES', 46, 'M', 'Vespertino', '248-234-4223', 'drcarlos@outlook.com')
SELECT * FROM DUAL;

/*INSERCION DE ROLES*/
INSERT ALL INTO ROL (nombre_rol) VALUES ('PACIENTE') 
           INTO ROL (nombre_rol) VALUES ('ESPECIALISTA')
           INTO ROL (nombre_rol) VALUES ('TECNICO ESPECIALISTA')
           INTO ROL (nombre_rol) VALUES ('ENFERMERA')
           INTO ROL (nombre_rol) VALUES ('VIGILANCIA')
           SELECT * FROM DUAL;


INSERT ALL INTO USUARIO (USERNAME, PASS, ID_ROL) VALUES('rojas333', '1232141', 2) 
           INTO USUARIO (USERNAME, PASS, ID_ROL) VALUES('carlos666', '85854949', 2)
           SELECT * FROM DUAL;

/**INSERCION DE ESPECIALISTA**/
INSERT INTO ESPECIALISTA (ID_USUARIO, id_perfil) VALUES(1, 1);
INSERT INTO ESPECIALISTA (ID_USUARIO, id_perfil) VALUES(2, 2);
           
/*INSERT ALL INTO ESPECIALISTA (ID_USUARIO, ID_PERFIL) 
VALUES(
INTO USUARIO(USERNAME, PASS, ID_ROL) 
       VALUES('rojas333', '1232141', 2), 
       INTO(PERFIL (NOMBRE, APELLIDOP, APELLIDOM, EDAD, SEXO, TURNO, TELEFONO, CORREO) 
       VALUES('JOSE', 'ROJAS', 'PASCUAL', 57, 'M', 'Matutino', '248-173-3833', 
       'pascualrojas@gmail.com')) 
SELECT * FROM DUAL;

*/




SELECT * FROM ESPECIALISTA E INNER JOIN USUARIO U  ON e.id_usuario = u.id_usuario 
INNER JOIN PERFIL P ON e.id_perfil = p.id_perfil
INNER JOIN ROL R ON U.ID_ROL = R.ID_ROL where id_especialista = 1;


CREATE OR REPLACE PROCEDURE NEWESPECIALISTA_01(USERNAME NVARCHAR2, PASS_ NVARCHAR2, ID_ROL_ NUMBER, 
                            NOMBRE_ NVARCHAR2, APELLIDOP_ NVARCHAR2, APELLIDOM_ NVARCHAR2, EDAD_ NUMBER, 
                            SEXO_ NVARCHAR2, TURNO_ NVARCHAR2, TELEFONO_ NVARCHAR2, CORREO_ NVARCHAR2)
                            IS
    id_u number := 0;
    id_p number := 0;
    id_e number := 0;
BEGIN
    
    INSERT INTO PERFIL (NOMBRE, APELLIDOP, APELLIDOM, EDAD, SEXO, TURNO, TELEFONO, CORREO) 
    VALUES(NOMBRE_, APELLIDOP_, APELLIDOM_, EDAD_, SEXO_, TURNO_, TELEFONO_, CORREO_)
    RETURNING id_perfil INTO id_p;
    -- RETURNING id_perfil INTO :id_p;
    
    INSERT INTO USUARIO (USERNAME, PASS, ID_ROL) VALUES(USERNAME_, PASS_, ID_ROL_)
    RETURNING id_usuario INTO id_u;
    
    INSERT INTO ESPECIALISTA (ID_USUARIO, id_perfil) VALUES(id_u, id_p)
    RETURNING id_especialista INTO id_e;
    
 /*   SELECT * FROM ESPECIALISTA E INNER JOIN USUARIO U  ON e.id_usuario = u.id_usuario 
    INNER JOIN PERFIL P ON e.id_perfil = p.id_perfil
    INNER JOIN ROL R ON U.ID_ROL = R.ID_ROL WHERE id_especialista = id_e; 
   */ 
END;
/



BEGIN
NEWESPECIALISTA_01('rivas1231', '393838847', 2, 
                   'Pedro', 'Rivas', 'Paredes', 44, 'M', 
                   'Vespertino', '258-232-2322', 'pedro@gmail.com'
                   );
                   


SELECT * FROM ESPECIALISTA E INNER JOIN USUARIO U  ON e.id_usuario = u.id_usuario 
    INNER JOIN PERFIL P ON e.id_perfil = p.id_perfil
    INNER JOIN ROL R ON U.ID_ROL = R.ID_ROL; --WHERE id_especialista = id_e; 
END;





/**INSERTAR EN LA TABLA SERVICIO-ESPECIALISTA**/
INSERT INTO SERVICIO_ESPECIALISTA (id_servicio, id_especialista) VALUES (1,1);


SELECT se.id_servicio_especialista, 
(p.nombre || ' ' || p.apellidop || ' ' || p.apellidom) as "Nombre Especialista" , 
r.nombre_rol, s.nombre_servicio, a.nombre_area, l.nombre_laboratorio
FROM SERVICIO_ESPECIALISTA SE 
INNER JOIN SERVICIO S ON se.id_servicio = s.id_servicio
INNER JOIN AREA A ON a.id_area = s.id_area
INNER JOIN laboratorio L ON l.id_laboratorio = a.id_laboratorio
INNER JOIN especialista E ON se.id_especialista = e.id_especialista
INNER JOIN USUARIO U ON u.id_usuario = e.id_usuario
INNER JOIN ROL R ON r.id_rol = u.id_rol
INNER JOIN PERFIL P ON p.id_perfil = e.id_perfil;




/**TABLAS TIPO_MUESTRA, ANALISIS Y SERV_ANALISIS**/
INSERT ALL INTO tipo_muestra (nombre_tipo) VALUES ('SANGUINEO')
       INTO tipo_muestra (nombre_tipo) VALUES ('HECES')
       INTO tipo_muestra (nombre_tipo) VALUES ('ORINA')
       SELECT * FROM DUAL;


INSERT ALL INTO analisis (nombre_analisis, id_tipo_muestra) VALUES ('Prueba ABO', 1)
INTO analisis (nombre_analisis, id_tipo_muestra) VALUES ('Prueba RH', 1)
INTO analisis (nombre_analisis, id_tipo_muestra) VALUES ('Anticuerpos Irregulares', 1)
INTO analisis (nombre_analisis, id_tipo_muestra) VALUES ('Prueba Pruebas Cruzadas', 1)
SELECT * FROM DUAL;


INSERT ALL INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (1,1) 
        INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (1,2)
        INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (1,3)
        INTO SERV_ANALISIS (id_servicio, id_analisis) VALUES (1,4)
        SELECT * FROM DUAL;





SELECT se.id_servicio_especialista, 
(p.nombre || ' ' || p.apellidop || ' ' || p.apellidom) as "Nombre Especialista" , 
r.nombre_rol, s.nombre_servicio, a.nombre_area, l.nombre_laboratorio,
ana.id_analisis, tm.nombre_tipo
FROM SERVICIO_ESPECIALISTA SE 
INNER JOIN SERVICIO S ON se.id_servicio = s.id_servicio
INNER JOIN AREA A ON a.id_area = s.id_area
INNER JOIN laboratorio L ON l.id_laboratorio = a.id_laboratorio
INNER JOIN especialista E ON se.id_especialista = e.id_especialista
INNER JOIN USUARIO U ON u.id_usuario = e.id_usuario
INNER JOIN ROL R ON r.id_rol = u.id_rol
INNER JOIN PERFIL P ON p.id_perfil = e.id_perfil
INNER JOIN serv_analisis SV ON sv.id_servicio = s.id_servicio
INNER JOIN ANALISIS ANA ON ana.id_analisis = sv.id_analisis
INNER JOIN TIPO_MUESTRA TM ON tm.id_tipo_muestra = ana.id_tipo_muestra;




